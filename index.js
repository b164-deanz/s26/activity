const http = require('http');
 
const port = 3000;

const server = http.createServer((request, response) => {
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Hello, Welcome to Login Page!')
	}
	else if(request.url == '/register'){
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end(`I'm sorry the page you are looking for cannot be found.`)
	}


})

server.listen(port)
console.log(`The server is succesfully running at localhost: ${port}`)